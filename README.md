# CurrencyConverter

An app to convert from any currency to any other one. The app relies on an API to fetch the rates of the current day. Users can also specify an amount in the conversion.

Demo Video : https://www.dropbox.com/s/mf7nqjwvn97ylcc/6-CurrencyConverter.mp4?dl=0