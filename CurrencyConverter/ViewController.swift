//
//  ViewController.swift
//  CurrencyConverter
//
//  Created by Ibraheem Hindi on 8/15/18.
//  Copyright © 2018 Ibraheem Hindi. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource{
    
    @IBOutlet weak var date_label: UILabel!
    @IBOutlet weak var picker: UIPickerView!
    @IBOutlet weak var result_label: UILabel!
    @IBOutlet weak var loader: UIActivityIndicatorView!
    @IBOutlet weak var amount_field: UITextField!
    
    var currencies = [String]()
    var suggestions = [String]()
    var currency_to_eur = Dictionary<String, Double>()
    
    
    func prepareSuggestions(query: String){
        suggestions = currencies.filter({(currency : String) -> Bool in
            return currency.lowercased().contains(query.lowercased())
        })
    }
    
    func showSimpleAlert(title: String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel) {UIAlertAction in})
        self.present(alert, animated: true, completion: nil)
    }
    
    func roundThreePlaces(num: Double, places: Int) -> Double{
        return Double(round(1000 * num) / 1000)
    }
    
    func updateResult(){
        var error = false
        let from = currencies[picker.selectedRow(inComponent: 0)]
        let to = currencies[picker.selectedRow(inComponent: 1)]
        var result = roundThreePlaces(num: (currency_to_eur[from]! / currency_to_eur[to]!), places: 3)
        
        if amount_field.text != ""{
            if let amount = Double(amount_field.text!){
                result = roundThreePlaces(num: result * amount, places: 3)
            }
            else{
                error = true
                showSimpleAlert(title: "Error", message: "Please enter a valid amount")
            }
        }
        
        if !error{
            result_label.text = "Result : \(result) \(to)"
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        picker.delegate = self
        picker.dataSource = self
        picker.setValue(UIColor.white, forKeyPath: "textColor")

        // Fetch currencies
        let url_string = "http://data.fixer.io/api/latest?access_key=43b60705a5de0feb314a7a2eed5106c6"
        fetchFromUrl(url_string: url_string)
    }

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return currencies.count
    }
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        
        return NSAttributedString(
            string: currencies[row],
            attributes: [
                .foregroundColor: UIColor.white
            ]
        )
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return currencies[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        updateResult()
    }
    
    func fetchFromUrl(url_string: String){
        let url = URL(string: url_string)
        let session = URLSession.shared
        
        let task = session.dataTask(with: url!) { (data, response, error) in
            if error != nil {
                self.showSimpleAlert(title: "Network Error", message: "Please check your internet connection")
            }
            else {
                if data != nil {
                    do {
                        let json_response = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! Dictionary<String, AnyObject>
                        
                        DispatchQueue.main.async {
                            self.onResponse(json_response: json_response)
                        }
                    }
                    catch {
                        print("Error while fetching from \(url_string)")
                    }
                }
                
            }
        }
        
        loader.startAnimating()
        task.resume()
    }
    
    func onResponse(json_response: Dictionary<String, AnyObject>){
        loader.stopAnimating()
        let rates = json_response["rates"] as! [String : AnyObject]
        
        for (key, value) in rates{
            currencies.append(key)
            currency_to_eur[key] = 1.0 / Double(truncating: value as! NSNumber)
        }
        
        // Set update date
        let date = json_response["date"] as? String
        var date_parts = date?.split(separator: "-")
        date_parts?.reverse()
        date_label.text = date_parts?.joined(separator: "-")

        // Setup currencies picker
        currencies.sort()
        picker.reloadAllComponents()
        picker.selectRow(currencies.count/2 - 1, inComponent: 0, animated: false)
        picker.selectRow(currencies.count/2, inComponent: 1, animated: false)
        updateResult()
    }
    
    @IBAction func onAmountEntered(_ sender: Any) {
        resignFirstResponder()
        updateResult()
    }
    
}
















